# helper
The basic idea was to make it easier to use TableView / CollectiobView,  throw away the extra work like: cell registration.
How this is work:
Your Cell Implement 2 Protocols NibLoadableView(this protocol must be when you create cell with XIB) and ReusabeView

```
final class CollectionViewCell: UICollectionCell, ReusableView, NibLoadableView {}
```
In DataSource method just use method from extension which need for your case 
```
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
let cell: CollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
return cell
}

```
